#include <QtWidgets>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTreeView tvw;
    QFileSystemModel fsm;

    fsm.setRootPath(QDir::rootPath());
    tvw.setModel(&fsm);

    QModelIndex index = fsm.index(QDir::currentPath());
    tvw.setRootIndex(index);
    tvw.show();

    /*QListWidget lwg;
    QListWidgetItem* pitem;
    QStringList lst;

    lwg.setIconSize(QSize(48, 48));
    lwg.setSelectionMode(QAbstractItemView::MultiSelection);
    lwg.setViewMode(QListWidget::IconMode);

    lst << "Linux" << "MacOS" << "Android" << "Windows";

    foreach (QString str, lst) {
        pitem = new QListWidgetItem(str, &lwg);
        pitem->setIcon(QPixmap(":/" + str));
    }

    lwg.show();*/

    return a.exec();
}
